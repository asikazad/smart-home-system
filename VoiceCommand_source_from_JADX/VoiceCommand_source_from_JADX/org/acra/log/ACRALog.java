package org.acra.log;

public interface ACRALog {
    int mo1607d(String str, String str2);

    int mo1608d(String str, String str2, Throwable th);

    int mo1609e(String str, String str2);

    int mo1610e(String str, String str2, Throwable th);

    String getStackTraceString(Throwable th);

    int mo1612i(String str, String str2);

    int mo1613i(String str, String str2, Throwable th);

    int mo1614v(String str, String str2);

    int mo1615v(String str, String str2, Throwable th);

    int mo1616w(String str, String str2);

    int mo1617w(String str, String str2, Throwable th);

    int mo1618w(String str, Throwable th);
}
